<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Model;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class Question
{

    /**
     *
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $question;

    /**
     *
     * @var bool $isMultiple
     */
    private $isMultiple;

    /**
     *
     * @var bool $isText
     */
    private $isText;

    /**
     *
     * @var Answer[] $answers
     */
    private $answers;

    /**
     *
     * @var Category $category
     */
    private $category;

    /**
     *
     */
    public function __construct()
    {
        $this->answers    = array();
        $this->isMultiple = false;
        $this->isText     = false;
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param integer $id
     * @return Question
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Gets the value of question.
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Sets the value of question.
     *
     * @param string $question the question
     *
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isMultiple()
    {
        return $this->isMultiple;
    }

    /**
     *
     * @return bool
     */
    public function isText()
    {
        return $this->isText;
    }

    /**
     *
     * @return Question
     */
    public function setMultiple()
    {
        $this->isMultiple = true;
        return $this;
    }

    /**
     *
     * @return Question
     */
    public function setText()
    {
        $this->isText = true;
        return $this;
    }

    /**
     *
     * @param Answer[] $answers
     * @return Question
     */
    public function setAnswers(array $answers)
    {
        $this->answers = $answers;
        return $this;
    }

    /**
     *
     * @return Answer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add answer.
     *
     * @param Answer $answer the answer
     *
     * @return Question
     */
    public function addAnswer(Answer $answer)
    {
        $this->answers[] = $answer;
        return $this;
    }

    /**
     * @param Category $category
     * @return Question
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }
}
