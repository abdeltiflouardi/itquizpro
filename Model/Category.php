<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Model;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class Category
{

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var string $name
     */
    private $name;

    /**
     *
     * @var string
     */
    private $description;

    /**
     *
     * @var Question[]
     */
    private $questions;

    /**
     * Initialize
     */
    public function __construct()
    {
        $this->questions = array();
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param integer $id
     * @return Category
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Gets the value of name.
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param string $name $name the name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     *
     * @param string $description
     * @return \ITQuizPro\Model\Category
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     *
     * @return Question[]
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     *
     * @param Question[] $questions
     * @return Category
     */
    public function setQuestions(array $questions)
    {
        $this->questions = $questions;
        return $this;
    }

    /**
     *
     * @param Question $question
     * @return Category
     */
    public function addQuestion(Question $question)
    {
        $this->questions[] = $question;
        return $this;
    }
}
