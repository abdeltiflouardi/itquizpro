<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Quiz;

use Exception;
use ReflectionClass;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizConfig implements QuizConfigInterface
{

    protected $options = array(
        'yml_files' => array(),
        'yml_dirs' => array(),
        'category' => null
    );

    /**
     *
     * @var array
     */
    public $providers = array(
        'ITQuizPro\Quiz\QuizProvider',
    );

    /**
     *
     * @var array
     */
    public $loaders = array(
        'ITQuizPro\Quiz\Loader\YamlLoader',
    );

    public function setOptions(array $options)
    {
        $this->options = $options;
        return $this;
    }

    public function addOption($key, $value)
    {
        $this->options[$key] = $value;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getOption($option, $default = null)
    {
        if (array_key_exists($option, $this->getOptions())) {
            return $this->options[$option];
        }

        return $default;
    }

    /**
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     *
     * @return QuizProviderInterface[]
     */
    public function resolveProviders()
    {
        return $this->resolveClasses($this->providers, '\ITQuizPro\Quiz\QuizProviderInterface');
    }

    /**
     *
     * @return Loader\LoaderInterface[]
     */
    public function resolveLoaders()
    {
        return $this->resolveClasses($this->loaders, '\ITQuizPro\Quiz\Loader\LoaderInterface');
    }

    /**
     *
     * @param array $classes
     * @param string $instanceOf
     * @return array
     * @throws Exception
     */
    public function resolveClasses(array $classes, $instanceOf)
    {
        $objects = array();

        // string to object transformers
        // Check if objects are instanceof $instanceOf
        foreach ($classes as $class) {
            $refClass = new ReflectionClass($class);

            if (!$refClass->implementsInterface($instanceOf)) {
                throw new Exception(sprintf('%s must be instanceof %s', $refClass->getName(), $instanceOf));
            }

            $objects[] = $refClass->newInstanceArgs();
        }

        return $objects;
    }
}
