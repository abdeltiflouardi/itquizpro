<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Model;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizQuestion
{

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var Quiz $quiz
     */
    private $quiz;

    /**
     *
     * @var Question
     */
    private $question;

    /**
     *
     * @var integer
     */
    private $timePassed;

    /**
     *
     * @var bool
     */
    private $isCorrect;

    /**
     *
     * @var QuizAnswer[] $quizAnswers
     */
    private $quizAnswers;

    /**
     * Initialize
     */
    public function __construct()
    {
        $this->quizAnswers = array();
        $this->isCorrect   = false;
    }

    /**
     *
     * @param integer $id
     * @return QuizQuestion
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param Quiz $quiz
     * @return QuizQuestion
     */
    public function setQuiz(Quiz $quiz)
    {
        $this->quiz = $quiz;
        return $this;
    }

    /**
     *
     * @return Quiz
     */
    public function getQuiz()
    {
        return $this->quiz;
    }

    /**
     *
     * @param Question $question
     * @return QuizQuestion
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     *
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     *
     * @param integer $timePassed
     * @return QuizQuestion
     */
    public function setTimePassed($timePassed)
    {
        $this->timePassed = $timePassed;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getTimePassed()
    {
        return $this->timePassed;
    }

    /**
     *
     * @return QuizQuestion
     */
    public function setCorrect()
    {
        $this->isCorrect = true;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isCorrect()
    {
        return $this->isCorrect;
    }

    /**
     *
     * @param QuizAnswer[] $quizAnswers
     * @return Question
     */
    public function setQuizAnswers(array $quizAnswers)
    {
        $this->quizAnswers = $quizAnswers;
        return $this;
    }

    /**
     *
     * @return QuizAnswer[]
     */
    public function getQuizAnswers()
    {
        return $this->quizAnswers;
    }

    /**
     * Add quizAnswer.
     *
     * @param QuizAnswer $quizAnswer the quizAnswer
     *
     * @return Question
     */
    public function addQuizAnswer(QuizAnswer $quizAnswer)
    {
        $this->quizAnswers[] = $quizAnswer;
        return $this;
    }
}
