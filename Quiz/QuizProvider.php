<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Quiz;

use ITQuizPro\Quiz\QuizMapper;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizProvider implements QuizProviderInterface
{
    protected $config;

    public function setConfig(QuizConfigInterface $config)
    {
        $this->config = $config;

        return $this;
    }

    public function loadData()
    {
        $questions = array();
        foreach ($this->config->resolveLoaders() as $loader) {
            $questions = $loader->load($this->config)->getItems();
        }

        return $this->mapData($questions);
    }

    public function mapData($questions)
    {
        return QuizMapper::transformFromArray($questions);
    }
}
