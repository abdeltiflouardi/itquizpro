<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Tests\Model;

use DateTime;
use ITQuizPro\Model\Answer;
use ITQuizPro\Model\Category;
use ITQuizPro\Model\Question;
use ITQuizPro\Model\Quiz;
use ITQuizPro\Model\QuizAnswer;
use ITQuizPro\Model\QuizQuestion;
use PHPUnit_Framework_TestCase;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizTest extends PHPUnit_Framework_TestCase
{

    public function testGetCategory()
    {
        $this->assertInstanceOf('ITQuizPro\Model\Category', $this->getQuiz()->getCategory());
    }

    public function testGetCreatedAt()
    {
        $this->assertInstanceOf('DateTime', $this->getQuiz()->getCreatedAt());
    }

    public function testGetId()
    {
        $this->assertEquals($this->getQuiz()->getId(), 1);
    }

    public function testGetScore()
    {
        $this->assertEquals($this->getQuiz()->getScore(), 100);
    }

    public function testGetTotalTimePassed()
    {
        $this->assertEquals($this->getQuiz()->getTotalTimePassed(), 60);
    }

    /**
     * @return Quiz
     */
    protected function getQuiz()
    {
        $quiz = new Quiz();
        $quiz->setRandomize();

        foreach ($this->getQuestions() as $question) {
            $quizQuestion = new QuizQuestion();
            $quizQuestion->setQuestion($question);

            $answer = $question->getAnswers();

            $quizAnswer = new QuizAnswer();
            $quizAnswer->setAnswer($answer[0]);

            $quizQuestion->addQuizAnswer($quizAnswer);

            $quiz->addQuizQuestion($quizQuestion);
        }

        return $quiz
                        ->setId(1)
                        ->setScore(100)
                        ->setTotalTimePassed(60)
                        ->setCategory($this->getCategory())
                        ->setCreatedAt(new DateTime());
    }

    /**
     *
     * @return Category
     */
    protected function getCategory()
    {
        return new Category();
    }

    /**
     * @return Question[]
     */
    protected function getQuestions()
    {
        $question = new Question();
        $answer   = new Answer();

        $question->addAnswer($answer);

        return array($question);
    }
}
