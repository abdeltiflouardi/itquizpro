<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Quiz;

use ITQuizPro\Model\Answer;
use ITQuizPro\Model\Category;
use ITQuizPro\Model\Question;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizMapper
{
    public static function transformFromArray(array $questions)
    {
        $mappedData = array();

        foreach ($questions as $categoryValue => $questionValues) {
            $category = self::transformCategory($categoryValue);

            foreach ($questionValues as $questionValue) {
                $question = self::transformQuestion($questionValue);

                self::transformAnswers($question, $questionValue['answers']);

                $category->addQuestion($question);
            }

            $mappedData[] = $category;
        }

        return $mappedData;
    }

    public static function transformCategory($value)
    {
        $category = new Category();
        $category->setName($value);

        return $category;
    }

    public static function transformQuestion($value)
    {
        $question = new Question();
        $question->setQuestion($value['question']);

        return $question;
    }

    public static function transformAnswers(Question $question, $values)
    {
        foreach ($values as $value) {
            $answer = new Answer();
            $answer->setAnswer($value['answer']);
            $answer->setCorrect($value['correct']);

            $question->addAnswer($answer);
        }
    }
}
