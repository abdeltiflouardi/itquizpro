<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Tests\Quiz\Loader;

use ITQuizPro\Quiz\Loader\YamlLoader;
use PHPUnit_Framework_TestCase;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class YmlLoaderTest extends PHPUnit_Framework_TestCase
{
    public function testLoadFromDirs()
    {
        $loader = $this->getYamlLoader();
        $loader->loadFromDirs($loader->getDirs());

        $this->assertCount(3, $loader->getItems());
    }

    public function testLoadFromFiles()
    {
        $loader = $this->getYamlLoader();
        $loader->loadFromFiles($loader->getFiles());

        $items = $loader->getItems();

        $this->assertTrue(array_key_exists('PHP', $items));
        $this->assertTrue(array_key_exists('C', $items));

        $this->assertCount(3, $items['PHP']);
    }

    public function testLoad()
    {
        $loader = $this->getYamlLoader();
        $loader->load();

        $this->assertCount(3, $loader->getItems());
    }

    public function getYamlLoader()
    {
        $files = array(
            sprintf('%s/../../fixtures2/c.yml', __DIR__),
            sprintf('%s/../../fixtures/php.yml', __DIR__),
            sprintf('%s/../../fixtures2/php2.yml', __DIR__)
        );

        $dirs = array(
            sprintf('%s/../../fixtures2/', __DIR__),
            sprintf('%s/../../fixtures/', __DIR__)
        );

        $loader = new YamlLoader();
        $loader
            ->setDirs($dirs)
            ->setFiles($files);

        return $loader;
    }
}
