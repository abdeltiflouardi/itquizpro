<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Tests\Model;

use ITQuizPro\Model\Answer;
use PHPUnit_Framework_TestCase;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class AnswerTest extends PHPUnit_Framework_TestCase
{

    public function testGetId()
    {
        $this->assertEquals($this->getAnswer()->getId(), 1);
    }

    public function testGetAnswer()
    {
        $this->assertEquals($this->getAnswer()->getAnswer(), 'PHP');
    }

    public function testisCorrectAnswer()
    {
        $answer = $this->getAnswer();

        $this->assertEquals($answer->isCorrect(), false);

        $answer->setCorrect(true);

        $this->assertEquals($answer->isCorrect(), true);
    }

    /**
     * @return Answer
     */
    protected function getAnswer()
    {
        $answer = new Answer();
        return $answer
            ->setId(1)
            ->setAnswer('PHP');
    }
}
