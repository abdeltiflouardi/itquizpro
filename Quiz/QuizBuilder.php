<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Quiz;

use ITQuizPro\Model\Quiz;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizBuilder
{

    /**
     *
     * @var QuizConfigInterface
     */
    protected $config;

    /**
     *
     * @return QuizConfigInterface
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     *
     * @param \ITQuizPro\Quiz\QuizConfigInterface $config
     * @return \ITQuizPro\Quiz\QuizBuilder
     */
    public function setConfig(QuizConfigInterface $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     *
     */
    public function build(Quiz $quiz)
    {
        $quizQuestions = array();
        foreach ($this->config->resolveProviders() as $provider) {
            $questions = $provider
                ->setConfig($this->getConfig())
                ->loadData();
        }

        $quiz->setQuizQuestions($quizQuestions);

        return $quiz;
    }

    /**
     *
     */
    public function getQuiz()
    {
        return $this->build();
    }
}
