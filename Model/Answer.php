<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Model;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class Answer
{

    /**
     *
     * @var integer $id
     */
    private $id;

    /**
     * @var string
     */
    private $answer;

    /**
     *
     * @var bool
     */
    private $isCorrect;

    /**
     *
     * @var Question $question
     */
    private $question;

    /**
     * Initialize
     */
    public function __construct()
    {
        $this->isCorrect = false;
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param integer $id
     * @return Answer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Gets the value of answer.
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Sets the value of answer.
     *
     * @param string $answer the answer
     *
     * @return Answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isCorrect()
    {
        return $this->isCorrect;
    }

    /**
     *
     * @param bool $correct
     * @return Answer
     */
    public function setCorrect($correct)
    {
        $this->isCorrect = $correct;
        return $this;
    }

    /**
     *
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     *
     * @param Question $question
     * @return Answer
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;
        return $this;
    }
}
