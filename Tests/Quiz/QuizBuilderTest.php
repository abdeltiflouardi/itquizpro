<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Tests\Quiz;

use ITQuizPro\Model\Quiz;
use ITQuizPro\Model\QuizInterface;
use ITQuizPro\Quiz\QuizBuilder;
use ITQuizPro\Quiz\QuizConfig;
use PHPUnit_Framework_TestCase;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizBuilderTest extends PHPUnit_Framework_TestCase
{

    public function testBuild()
    {
        $builder = $this->getQuizBuilder();

        $quiz = new Quiz();
        $builder->build($quiz);

        $this->assertTrue($quiz instanceof QuizInterface);
    }

    public function getQuizBuilder()
    {
        $dirs = array(
            sprintf('%s/../fixtures2/', __DIR__),
            sprintf('%s/../fixtures/', __DIR__)
        );

        $config = new QuizConfig();
        $config->addOption('yml_dirs', $dirs);
        $config->addOption('category', 'PHP');

        $builder = new QuizBuilder();
        $builder->setConfig($config);

        return $builder;
    }
}
