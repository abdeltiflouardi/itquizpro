<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Quiz\Loader;

use ITQuizPro\Quiz\QuizConfigInterface;
use SplFileInfo as File;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class YamlLoader implements LoaderInterface
{
    private $category;
    private $dirs;
    private $files;
    private $items;

    public function __construct()
    {
        $this->items = array();
        $this->files = array();
        $this->dirs  = array();
    }

    public function setDirs(array $dirs)
    {
        $this->dirs = $dirs;
        return $this;
    }

    public function getDirs()
    {
        return $this->dirs;
    }

    public function setFiles(array $files)
    {
        $this->files = $files;
        return $this;
    }

    public function getFiles()
    {
        return $this->files;
    }

    public function setItems(array $items)
    {
        $this->items = $items;
        return $this;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function loadFromDirs($dirs)
    {
        $finder   = new Finder();
        $iterator = $finder
            ->files()
            ->name('*.yml')
            ->depth(0)
            ->in($dirs);

        $this->loadFromFiles($iterator);

        return $this;
    }

    /**
     *
     * @param array|Iterator $files
     */
    public function loadFromFiles($files)
    {
        foreach ($files as $file) {
            $file = is_object($file) ? $file : new File($file);

            $this->loadFromFile($file);
        }

        return $this;
    }

    /**
     *
     * @param File $file
     */
    public function loadFromFile(File $file)
    {
        $this->format(Yaml::parse($file, true, true));

        return $this;
    }

    public function format($data)
    {
        $category = $data['category'];
        if ($this->category && $category != $this->category) {
            return $this;
        }

        $questions = $data['questions'];

        if (array_key_exists($category, $this->items)) {
            $this->items[$category] = array_merge($this->items[$category], $questions);
        } else {
            $this->items[$category] = $questions;
        }

        return $this;
    }

    public function load(QuizConfigInterface $config = null)
    {
        $this->resolveOptions($config);

        $this->loadFromDirs($this->getDirs());
        $this->loadFromFiles($this->getFiles());

        return $this;
    }

    public function resolveOptions(QuizConfigInterface $config = null)
    {
        if (!$config) {
            return;
        }

        if ($files = $config->getOption('yml_files')) {
            $this->setFiles($files);
        }

        if ($dirs = $config->getOption('yml_dirs')) {
            $this->setDirs($dirs);
        }

        $this->category = $config->getOption('category');
    }
}
