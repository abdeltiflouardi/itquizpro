<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Tests\Quiz;

use ITQuizPro\Quiz\QuizConfig;
use PHPUnit_Framework_TestCase;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizConfigTest extends PHPUnit_Framework_TestCase
{

    public function testResolveProviders()
    {
        $providers = $this->getQuizConfig()->resolveProviders();
        $this->assertTrue(is_array($providers));

        $this->assertGreaterThan(0, count($providers), 'You must define at least 1 provider.');
    }

    public function getQuizConfig()
    {
        return new QuizConfig();
    }
}
