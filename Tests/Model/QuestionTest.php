<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Tests\Model;

use ITQuizPro\Model\Answer;
use ITQuizPro\Model\Question;
use PHPUnit_Framework_TestCase;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuestionTest extends PHPUnit_Framework_TestCase
{

    public function testGetQuestion()
    {
        $this->assertEquals($this->getQuestion()->getQuestion(), 'Zend is a framework?');
    }

    public function testGetAnswer()
    {
        $this->assertEquals(count($this->getQuestion()->getAnswers()), 4);
    }

    public function testSetAnswer()
    {

    }

    /**
     * @return Question
     */
    public function getQuestion()
    {
        $question = new Question();
        $question->setQuestion('Zend is a framework?');

        $answer1 = new Answer();
        $answer1->setAnswer('PHP');

        $answer2 = new Answer();
        $answer2->setAnswer('Perl');

        $answer3 = new Answer();
        $answer3->setAnswer('Java');

        $answer4 = new Answer();
        $answer4->setAnswer('Python');

        $question
                ->addAnswer($answer1)
                ->addAnswer($answer2)
                ->addAnswer($answer3)
                ->addAnswer($answer4);

        return $question;
    }
}
