<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Quiz\Loader;

use ITQuizPro\Quiz\QuizConfigInterface;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
interface LoaderInterface
{
    public function format($data);
    public function load(QuizConfigInterface $config);
    public function getItems();
}
