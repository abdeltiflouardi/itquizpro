<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Tests\Model;

use ITQuizPro\Model\Category;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class CategoryTest extends \PHPUnit_Framework_TestCase
{

    public function testGetId()
    {
        $category = $this->getCategory();

        $this->assertEquals($category->getId(), 1);
    }

    public function testGetName()
    {
        $category = $this->getCategory();

        $this->assertEquals($category->getName(), 'Web Developper');
    }

    public function testGetDescription()
    {
        $category = $this->getCategory();

        $this->assertEquals($category->getDescription(), 'All questions of web developpement.');
    }

    /**
     * @return Category
     */
    protected function getCategory()
    {
        $category = new Category();

        return $category->setId(1)
                        ->setName('Web Developper')
                        ->setDescription('All questions of web developpement.');
    }
}
