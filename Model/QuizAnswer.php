<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Model;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class QuizAnswer
{

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var QuizQuestion
     */
    private $quizQuestion;

    /**
     *
     * @var Answer
     */
    private $answer;

    /**
     *
     * @var string
     */
    private $text;

    /**
     *
     * @param integer $id
     * @return QuizAnswer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return QuizQuestion
     */
    public function getQuizQuestion()
    {
        return $this->quizQuestion;
    }

    /**
     *
     * @param QuizQuestion $quizQuestion
     * @return QuizAnswer
     */
    public function setQuizQuestion(QuizQuestion $quizQuestion)
    {
        $this->quizQuestion = $quizQuestion;
        return $this;
    }

    /**
     *
     * @param Answer $answer
     * @return QuizAnswer
     */
    public function setAnswer(Answer $answer)
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     *
     * @return Answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     *
     * @param string $text
     * @return QuizAnswer
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}
