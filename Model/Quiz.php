<?php

/*
 * This file is part of the library ITQuizPro.
 *
 * (c) Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ITQuizPro\Model;

use DateTime;

/**
 * @author Abdeltif LOUARDI <louardi.abdeltif@gmail.com>
 */
class Quiz implements QuizInterface
{

    /**
     *
     * @var integer
     */
    private $id;

    /**
     * Total time passed of test (in second)
     *
     * @var integer $totalTimePassed
     */
    private $totalTimePassed;

    /**
     * Get randomized question list
     *
     * @var bool
     */
    private $randomize;

    /**
     *
     * @var integer
     */
    private $score;

    /**
     *
     * @var DateTime
     */
    private $createdAt;

    /**
     *
     * @var Category
     */
    private $category;

    /**
     *
     * @var QuizQuestion[] $quizQuestions
     */
    private $quizQuestions;

    /**
     * Initialize
     */
    public function __construct()
    {
        $this->randomize     = false;
        $this->quizQuestions = array();
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param integer $id
     *
     * @return Quiz
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getTotalTimePassed()
    {
        return $this->totalTimePassed;
    }

    /**
     *
     * @param integer $totalTimePassed
     * @return Quiz
     */
    public function setTotalTimePassed($totalTimePassed)
    {
        $this->totalTimePassed = $totalTimePassed;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isRandomize()
    {
        return $this->randomize;
    }

    /**
     * Randomize of question
     *
     * @return Quiz
     */
    public function setRandomize()
    {
        $this->randomize = true;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     *
     * @param integer $score
     * @return Quiz
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }

    /**
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @param DateTime $createdAt
     * @return Quiz
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     *
     * @param Category $category
     * @return Quiz
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     *
     * @return QuizQuestion[]
     */
    public function getQuizQuestions()
    {
        return $this->quizQuestions;
    }

    /**
     *
     * @param QuizQuestion[] $quizQuestions
     * @return Quiz
     */
    public function setQuizQuestions(array $quizQuestions)
    {
        $this->quizQuestions = $quizQuestions;
        return $this;
    }

    /**
     *
     * @param QuizQuestion $quizQuestion
     * @return Quiz
     */
    public function addQuizQuestion(QuizQuestion $quizQuestion)
    {
        $this->quizQuestions[] = $quizQuestion;
        return $this;
    }
}
